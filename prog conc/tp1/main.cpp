#include <stdio.h>      
#include <stdlib.h>     
#include <thread>
#include <iostream>
#include<cstdlib>
#include <mutex>
#include <condition_variable>

using namespace std;

int cmp = 0;
mutex m;
std::condition_variable cond;

void fibo(){
    m.lock();
    cmp ++;
    m.unlock();

    srand(time(0));
    int n = std::rand() % 10;
    int first = 0;
    int second = 1;
    int tmp;
    
    for (int i = 0;  i < n; i ++) {
        tmp = first + second;
        first = second;
        second = tmp;
    }
    std::cout << first << std::endl;

    m.lock();
    if (cmp == 2 ){
        cond.notify_one();
    }
    m.unlock();
       
}

void tacheB(){
    std::unique_lock<std::mutex> lck(m);
    while (cmp != 2 ){
        std::cout << cond << std::endl;
        cond.wait(lck);
    }
    cmp = 0;
    std::cout << "B = " << cmp << std::endl;
}

int main (int argc, char ** argv) {
    
    for (int i = 0; i < 15; i++) {
        std::thread A_1(fibo);
        std::thread A_2 (fibo);
        std::thread B (tacheB);
        A_1.join (); A_2.join (); B.join ();
    }
    return 0;
}